"""
Arithmetics
"""
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, CENTER, ROW

from .arithmetics_window import new_arithmetics_window


class MathNinjaApp(toga.App):

    def startup(self):
        main_box = toga.Box(style=Pack(direction=COLUMN, background_color='#FF00FF'))

        # name_label = toga.Label(
        #     'Your name: ',
        #     style=Pack(padding=(0, 5))
        # )
        # self.name_input = toga.TextInput(style=Pack(flex=1))

        # name_box = toga.Box(style=Pack(direction=ROW, padding=5))
        # name_box.add(name_label)
        # name_box.add(self.name_input)

        button = toga.Button(
            'Start Training!',
            on_press=self.say_hello,
            style=Pack(padding_top=5, padding_bottom=5, padding_right=350, padding_left=350, alignment=CENTER)
        )

        # main_box.add(name_box)
        main_box.add(button)

        self.main_window = toga.MainWindow(title=self.formal_name)
        self.main_window.content = main_box
        self.main_window.show()

    def say_hello(self, widget):
        # self.windows[0].show()
        pass


def main():
    ar_window = new_arithmetics_window()
    return MathNinjaApp(windows=(ar_window,))
